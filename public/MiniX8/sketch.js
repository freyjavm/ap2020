let what = 0;
let is = 0;
let the = 0;
let meaning = 0;
let of = 0;
let our = 0;
let lives = 0;

let pseudoRandom;
let seriousness;

function preload() {
  pseudoRandom = loadJSON('randomWords.json');
  seriousness = loadImage("data/flowers.gif");
}

function setup() {
  createCanvas(800, 800);
}

function draw() {
  image(seriousness, 0, 0, 800, 800);

  push();
    noStroke();
    fill(3, 82, 252, 130);
    rectMode(CENTER);
    rect(width/2, 440, 500, 200);
  pop();

  title();
  poem();

}

function title(){
  textFont('Avenir');
  textSize(60);
  textAlign(CENTER);
  fill(255);
  text("The meaning of your life", width/2, height/2 - 110);
}

function poem(){
  // this variable is containing a path to data from the json file
  let everythingIsRandom = pseudoRandom.randomWords;
  textSize(40);
  textAlign(CENTER);
  text(everythingIsRandom[2].words[what] + everythingIsRandom[4].words[is], width/2, height/2 - 10);
  text(everythingIsRandom[5].words[the] + everythingIsRandom[3].words[meaning] + everythingIsRandom[1].words[of], width/2,height/2 + 50);
  text(everythingIsRandom[6].words[our] + "the " + everythingIsRandom[0].words[lives], width/2,height/2 + 110);

  // conditional statement to choose new random words
  if (frameCount > 300) {
    what = floor(random(20));
    is = floor(random(20));
    the = floor(random(20));
    meaning = floor(random(20));
    of = floor(random(20));
    our = floor(random(20));
    lives = floor(random(20));
    frameCount = 0;
    }
  }
