function setup() {
  // 3rd dimension added with WEBGL
  createCanvas(windowWidth,windowHeight, WEBGL);

  // 20 frames pr. second to make rotation slow
  frameRate(20)
}

function draw() {

  // Rectancle to make pattern, as it would not work with background() in 3D
  push();
  // 0,0 moved to top left corner
  translate(-windowWidth/2, -windowHeight/2, 0)
  fill(0, 200, 200, 2)
  noStroke();
  rect(0, 0, windowWidth, windowHeight)
  pop();

  // The rotation
  rotateX(frameCount * 0.05);
  rotateY(frameCount * 0.05);

  // Dots
  fill(200, 23, 255);
  stroke(200, 23, 255);
  strokeWeight(3);

  // for loop making ellipses between x = -250 and < 250
  // with the distance of 80 pixels
  for (let x = -250; x < 250; x = x + 80) {
    ellipse(x, 0, 10, 10);
    }

}
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
