<h2>Relaxing Throbber</h2>

![Screenshot](MiniX3.png)

<h4>Description of throbber design - conceptually and technically.</h4>

- **What is your sketch? What do you want to explore and/or express?**  
        My sketch forms a throbber where seven purple dots are rotating and leaving a light blue trail of dots that forms into a symmetric  
        pattern. The code behind my program is made in 3D with WEBGL in createCanvas(). The dots are formed by ellipses made in a for-loop,  
        where they are drawn when x = -250 (in WEBGL (0,0) is in the center of the canvas) and up to x = 250 with the distance of 80 pixels.  
        I wanted the dots to move in a satisfying way, so I looked into different 3D elements in p5.js reference and then I found out that  
        you combinate rotateX() and rotateY() to make an object rotate around both the x- and y-axis.  
        To make the trail left after the dots, I had to draw a rectancle with an alpha value on 2 so you can only see the color of the  
        rectancle where the dots are. I used a rectancle as you can't use backgruond when it's in 3D. As (0,0) is in the center of the canvas  
        I had to use translate() with push() and pop() to make the rectancle starting in the top left corner.  
- **What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is**  
  **time being constructed in computation (refer to both the reading materials and your process of coding)?**  
        To make the dots spin around in the pattern I used the frame count function and timed it with 0,05, when I had to define the ancle  
        of the rotation. The (frameCount * 0,05) makes the distance from the dots as the frame count increases over time and the 0,05 adds  
        5% of the frame count to the ancle that rotates the object. Therefore the ellipse moves a little draw by draw. At the standard  
        frame rate the dots spin around quite fast, so for that reason I've put the frame rate down to 20 frames pr. second. This function  
        helps me slowing down the speed of the rotations of the balls, as the program draws less ellipses pr. second than the usual frame  
        rate on 60 frames pr. second.  
- **Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest**  
  **feeds Facebook, or waiting for a ticket transaction, and consider what a throbber communicates, and/or hides? How might**  
  **we characterise this icon differently?**  
        The vision behind my throbber is to make the user forget time and the frustrations that usually come with a throbber's appearance  
        on a screen. A throbber is meant to make the use of internet smooth, but what often happens is that it turns out to feel like the  
        opposite. What I think is lying behind this issue is that a throbber often reminds you that there is a problem, and you don't know  
        when it's going to be solved - it does not indicate much else than that. I wanted to adress this issue of a throbber being a stressing  
        element and try to turn the focus away from the problem. When designing my throbber I tried to make it aetheticly pleasing with a  
        symmetric pattern being drawn in a calming rythm as time goes. 

<h4>Links: [RUNME](https://freyjavm.gitlab.io/ap2020/MiniX3/), [Sketch](https://gitlab.com/freyjavm/ap2020/-/blob/master/public/MiniX3/sketch.js), [Index](https://gitlab.com/freyjavm/ap2020/-/blob/master/public/MiniX3/index.html)</h4>