let num = 9
let makeSpikes = 0
let wheelSize = {
  w:86,
  h:89
};
let fiveSpikes;
let wheel;
let wheelPosY;
let wheelPosX;
let spike = [];
let min_spike = 5;

// Preloaded gif of spinning wheel
function preload(){
  wheel = loadImage("data/wheel.gif");
  fiveSpikes = loadImage("data/fiveSpikes.png");
}

function setup() {
  createCanvas(1000, 600);
  frameRate(50)

  // Position of gif
  wheelPosX = 50
  wheelPosY = height-90;

  // Create and contruct spike
  for(let i = 0; i <= min_spike; i++){
    spike[i] = new Spike();
  }
}

function draw() {
  background(236, 120, 123);

  // Score text
  textSize(30);
  textAlign(CENTER);
  text(frameCount, width / 2, height / 2 - 150);
  text('SCORE', width / 2, height / 2 - 200);

  moreSpikes();
  showSpike(); // Calling function showSpike
  image(wheel, wheelPosX, wheelPosY, wheelSize.w, wheelSize.h); // Drawing wheel
  checkSpikeNum(); //available spike


}
// Function that adds 1 to makeSpikes, so there will be space between spikes
function moreSpikes() {
  if ((frameCount%num) > 3) {
    makeSpikes++
  }
}
function showSpike() {
  for(let i = 0; i < makeSpikes; i++){
    spike[i].move();
    spike[i].show();
    if (dist(wheelPosX + wheelSize.w/2, wheelPosY + wheelSize.h/2, spike[i].pos.x, spike[i].pos.y) < 40) {
      gameOver();
    }
    // for (let i = 0; i<spike.length; i++) {
    // let d = dist(wheelPosX, wheelPosY, spike[i].pos1.x, spike[i].pos1.y); // Distance between wheel and spike
      // if (d < wheelSize.w) { // if the distance between the wheel and the spike is less than the width of the wheel, the game is over
  }
}

function checkSpikeNum() {
  if (spike.length < min_spike) {
    spike.push(new Spike());
  }
}

function keyPressed(){
  if(keyCode===32){
    wheelPosY = wheelPosY-300;
  }
}

function keyReleased(){
  if(keyCode===32){
    wheelPosY = height-90;
  }
}

function gameOver() {
    noLoop();
    }

class Spike {
  constructor(x,y) {
  this.speed = 10;
  this.pos = new createVector(width+110, height);
  // this.pos2 = new createVector(width+120, height-200);
  // this.pos3 = new createVector(width+130, height);
  }

  move() {
    this.pos.x -= this.speed; // this.pos.x = this.pos.x - this.speed

  }

  show() {
    image(fiveSpikes, this.pos.x, this.pos.y, 50, 50);
  }
}
