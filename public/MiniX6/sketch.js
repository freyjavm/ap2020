let wheelSize = {
  w:86,
  h:89
};
let wheel;
let wheelPosY;
let wheelPosX;
let spike = [];
let min_spike = 5;

// Preloaded gif of spinning wheel
function preload(){
  wheel = loadImage("data/wheel.gif");
}

function setup() {
  createCanvas(1000, 600);
  frameRate(50)

  // Position of gif
  wheelPosX = 50
  wheelPosY = height-90;

  // Create and contruct spike
  for(let i = 0; i <= min_spike; i++){
    spike[i] = new Spike();
  }
}

function draw() {
  background(236, 120, 123);

  // Score text
  textSize(30);
  textAlign(CENTER);
  text(frameCount, width / 2, height / 2 - 150);
  text('SCORE', width / 2, height / 2 - 200);

  showSpike(); //
  image(wheel, wheelPosX, wheelPosY, wheelSize.w, wheelSize.h);
  checkSpikeNum(); //available spike
}

function showSpike() {
  for(let i = 0; i < spike.length; i++){
    spike[i].move();
    spike[i].show();
  }
}

function checkSpikeNum() {
  if (spike.length < min_spike) {
    spike.push(new Spike());
  }
}

function keyPressed(){
  if(keyCode===32){
    wheelPosY = wheelPosY-300;
  }
}

function keyReleased(){
  if(keyCode===32){
    wheelPosY = height-90;
  }
}

function gameOver() {
  for (let i = 0; i<spike.length; i++) {
  let d = dist(wheelPosX + wheelSize.w/2, wheelPosY + wheelSize.h/2, spike[i].pos1.x, spike[i].pos1.y); //check the distance between
    if (d < wheelSize.w) { // if the distance between the feather and the bird is less than the width of the bird, the feather disappears
    // spike.splice(i,1);
    noLoop();
  } else if (spike[i].pos1.x < floor(0.2)) { // if the bird doesn't catch the feather, the feather still disappears
    // spike.splice(i,1);
  }
  }
}

class Spike {
  constructor() {
  this.speed = 10;
  this.pos1 = new createVector(width+110, height);
  this.pos2 = new createVector(width+120, height-200);
  this.pos3 = new createVector(width+130, height);
  }

  move() {
    this.pos1.x -= this.speed; // this.pos1.x = this.pos1.x - this.speed
    this.pos2.x -= this.speed; // this.pos2.x = this.pos2.x - this.speed
    this.pos3.x -= this.speed; // this.pos3.x = this.pos3.x - this.speed
  }

  show(){
    fill(0)
    triangle(this.pos1.x, this.pos1.y, this.pos2.x, this.pos2.y, this.pos3.x, this.pos3.y);
  }
}
