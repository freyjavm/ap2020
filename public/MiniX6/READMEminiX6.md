<h2>Spinning Wheel</h2>

![Screenshot](miniX6.png)

<h4>Links: [RUNME](https://freyjavm.gitlab.io/ap2020/MiniX6/), [sketch](https://gitlab.com/freyjavm/ap2020/-/blob/master/public/MiniX6/sketch.js), [sketch_notWorking](https://gitlab.com/freyjavm/ap2020/-/blob/master/public/MiniX6/sketch_notWorking.js), [index](https://gitlab.com/freyjavm/ap2020/-/blob/master/public/MiniX6/index.html)</h4>

**Describe how does your game/game objects work?**  
The aim of my game is to get a spinning wheel jumping over some spikes coming and by this getting a high score when doing this  
as long as possible. The score is determined from the frameCount running from start of program to when the wheel hit a spike.  
Unfortunately, I was not able to make my program run the way I wanted it to. There is only on spike approaching, it does not   
come again, when it is out of the canvas, and when you hit the spike, the game is not over. So, when you see the program running  
right now, it just shows a wheel that can jump with pressing spacebar and one spike that you are supposed to jump over. I tried  
several things to make the wheel react to the spike, but I could not get the dist(); working properly. Therefore, I have  
attached two sketch files, where “sketch” is the file connected to the RUNME and “sketch_notWorking” is where I tried making  
it work in other ways…  

**Describe how you program the objects and their related attributes and methods in your game.**  
In my program I have made the spikes as an object in a class. These spikes have a form made in show(); with triangle();, and a  
way of being in move();. Apart from that, they have a start position that is being subtracted by a speed that enables the spikes  
to move from right to left, so they approach the wheel. 

**What are the characteristics of object-oriented programming and the wider implications of abstraction?**  
According to Fuller and Goffrey in “How To Be a Geek” object-oriented programming or OOP is when you define groups of data into  
certain objects and program interactions between these different objects. They talk about that computing does not have the  
desire to tell us everything about the nature of things, it rather forms a universality to objects. 

In the text they talk about that object-oriented programming has the ability to re-use lines code to avoid re-writing of several  
lines in different programs. This is advantageous, as it allows to re-use code in different programs and a way of doing this is  
creating a class library for an object. This class would have the predefines properties and actions that categorizes the object  
as a certain thing. In general, this way of programming gives objects a regularized and stable way of behaving.  
What then comes with OOP is encapsulation, which is when data and information about objects are being hidden as they are “not  
important” for the abstraction and the idea about the object for the program to run in a certain way. Fuller and Goffrey argues  
that encapsulation works to stabilize abstraction, but also to minimize the risk of errors being created by incompetent  
programmers that tries to manipulate the program.  
Implications of abstractions are that an object are being defined (encapsulated) only be properties that are important for the  
program to run, and one way an object is perceived is potentially not the same for another.  

**Extend/connect your game project to wider digital culture context, can you think of a digital example and describe how**  
**complex details and operations are being abstracted?**  
My program is not directly connected critically to wider digital culture context in the sense that my object is only a spike – a  
triangle. Even though this is not a very generalizing object, this new way of thinking through programming as a practice of  
objectifying things and giving them properties, certainly connect to how OOP has an effect on digital culture as it is an approach  
where properties of object are being hidden. A wider example would be the new emojies from apple where they engage you to design  
your own emoji (which is properly a product of emojies being further and further developed into being universal (Snelting, 2018)).  
This is an example of how far out the design of properties can be developed, but even after individualizing the emoji this far,  
there are still properties left out, for unknown reasons, but this limited amount of properties affect the abstraction of the emoji  
in such way that you still see these individual emojies as universal.  
When talking about encapsulation, nem-ID login could be seen as a programmed object. As an object, is has properties (e.g. boxes  
you can type in) and methods (ways they check if the password is correct) lying encapsulated in a “black box”. The user of the  
platform and the developer of the platform are not supposed to be able to either know what data is being processed or be able to  
manipulate how it is functioning. This example is much wider to what we are doing in Aesthetic Programming but is shows how you can  
see something as an object, where the way it is programmed is hidden away. 
