// CHAOTIC SOLAR SYSTEM

function setup() {

  createCanvas(windowWidth, windowHeight, WEBGL);

}

function draw() {

  // Sun
  rotateX(millis() / 3000);
  rotateY(millis() / 3000);
  fill(179, 59, 87);
  sphere(100);

  // Mercury
  rotateX(millis() / 4000);
  rotateY(millis() / 3000);
  fill(179, 59, 100);
  ellipse(150,10,20,20);

  // Venus
  rotateX(millis() / 2000);
  rotateY(millis() / 3000);
  fill(179, 76, 125);
  ellipse(200,10,25,25);

  // Earth
  rotateX(millis() / 4000);
  rotateY(millis() / 3000);
  fill(179, 76, 151);
  ellipse(250,10,30,30);

  // Mars
  rotateX(millis() / 4000);
  rotateY(millis() / 3000);
  fill(179, 101, 151);
  ellipse(300,10,20,20);

  // Jupiter
  rotateX(millis() / 4000);
  rotateY(millis() / 3000);
  fill(170, 101, 151);
  ellipse(360,10,70,70);

  // Saturn
  rotateX(millis() / 4000);
  rotateY(millis() / 3000);
  fill(109, 101, 151);
  ellipse(450,10,60,60);

  // Uranus
  rotateX(millis() / 4000);
  rotateY(millis() / 3000);
  fill(75, 90, 151);
  ellipse(500,10,30,30);

  // Neptune
  rotateX(millis() / 4000);
  rotateY(millis() / 3000);
  fill(102, 106, 179);
  ellipse(550,10,30,30);
  
}
