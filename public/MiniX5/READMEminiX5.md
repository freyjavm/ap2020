<h2>SENSE IN CHAOS</h2>

![Screenshot](miniX5.png)

<h4></h4>

**What is the concept of your work? What is the departure point? What do you want to express? What have you changed and why?**  

In my miniX1 I made a solar system([Link](https://freyjavm.gitlab.io/ap2020/MiniX1/)), but there were a few things where I had to compromise back when I made it. The program was  
made in WEBGL, had a sun in the middle (made with sphere()), and eight ellipses rotating around the sun (the Y-axis) simulating the  
eight planets in our solar system. Firstly, I warned to work with this program, to make the code better, by making functions instead  
of one line of code in draw, but I had a lot of struggle doing this. When I made every planet into a function, the rotation would  
not work. I googled a lot and tried running other people’s code of solar systems, but nothing would work. After many hours of struggle,  
I realized that I had to come up with something new, so do the assignment. Then I ended up trying to combine my miniX1 with the  
throbber I made in miniX3. The concept of my work is a throbber where you are trying to make sense of the chaos. My vision was to make  
the viewer recognize that it looks somehow like a solar system, but because of rotations that doesn’t make sense. In my process of  
making a chaotic solar system I tried putting in another type of rotation (rotationX()), which I also did in my miniX3. Afterwards I  
accidently removed my background, which had super chaotic consequences. I found this outcome somehow interesting, because it simulates  
how chaotic the situation can be when a throbber appears. 

**Reflect upon what Aesthetic Programming might be (Based on the readings that you have done before (especially this week on**  
**Aesthetic Programming as well as the class01 on Why Program? and Coding Literacy), What does it mean by programming as a**  
**practice, or even as a method for design? Can you draw and link some of the concepts in the text and expand with your**  
**critical take? What is the relation between programming and digital culture?)**  

According to Nick Montfort programming allows people to think in new ways, it gives a better understanding of media culture and  
it can help us creating a better world by for example make utopia simulations to engage in discussions about how our world is  
built socially. It allows us to expand the diversity of methods in creative processes and open us up for other perspectives  
through “exploratory programming”. He even says that there have been studies showing that programming develops humans cognitively  
and improves human’s logical and general solving skills. Montfort brings up arguments on why you should exploratively program and  
use computers to create powerful and persuasive models instead of seeing it as a “black box”.  

While Montfort talks about why programming, Annette Vee addresses arguments on why to make programming as a literacy more  
accessible for everybody. Vee argues this by underlining that coding literacy gives individual empowerment in terms of personal  
expression; you learn new ways of thinking (logically) and it allows to reflect on your own thinking; collective progress in the  
sense that it is essential for citizens to manipulate and understand code; and employability.

More recently we have read a preface to “Aesthetic Programming: A Handbook of Software Studies”, where Winnie Soon and Geoff Cox  
point towards the field Aesthetic programming being important, as you need to explore into new insight in aesthetics and critical  
thinking by using fundamental concepts of programming. They argue that by programming as an artistic practice will open up for  
more speculative and alternative approaches. 

With the views combined in these three texts, I see a great importance of Aesthetic Programming. The way I see the field in my  
own process the past weeks, is that multiple times programming has given me new perspectives on the theme week by week – just by  
externalising small visions, developing them and get inspired by what is being executed. When you explore the relations between  
input and output a new dimension to creativity forms.



<h4>Links: [RUNME](https://freyjavm.gitlab.io/ap2020/MiniX5/), [Sketch](https://gitlab.com/freyjavm/ap2020/-/blob/master/public/MiniX5/sketch.js), [Index](https://gitlab.com/freyjavm/ap2020/-/blob/master/public/MiniX5/index.html)</h4>
