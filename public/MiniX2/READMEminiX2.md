**README on MiniX2 by Freyja**

![Screenshot](miniX2.png)

In this exercise I have made two emoijies, where one is wearing a hijab and one is wearing a burqa.  
Furthermore you have the possibility of choosing between 9 different colors on the hijab/burqa. 

I wanted to make an emoji that wasn't respresented in the emoji repertoir on my Apple iPhone.  
What I did to find inspiration was that I simply looked through the emojies on my iPhone, and  
while browsing, I came aross the current emoji that is wearing a hijab. I found it quiet limited,  
as it could only appear in one color, which was purple. [Link to current hijab emoji](https://www.aljazeera.com/mritems/imagecache/mbdxxlarge/mritems/Images/2017/7/18/d2e28e44190b40978c263abb592463e5_18.jpg)  
Then I started thinking why the developers chose purple, and how it may be effecting how people  
are using it. A woman using hijab, I believe would use it less, if she didn't like the color purple.  
Therefore I made a hijab emoji where you can choose the color. From this idea, an idea of a burqa  
emoji evolved. An emoji wearing a burqa is not respresented at all, and I find it interesting  
thinking about what the consequences might be if this emoji was added, as there are different  
political opinions and regulations for and against burqas.  

In the assigned reading "Modifying the Universal" by Femke Snelting, she talks about when you change  
something in the unicode repertoir, it is a never ending process in the modification. After making  
this I started thinking, why can't you just put a hijab on every emoji, as the hijab emoji only has  
one expression. But with the thought of neverending modifications in the back of my mind, I knew  
that my emoji couldn't follow every modification possibility. Orinally the hijab emoji has the  
possibility to change the skin color, and this I have kept out and made the skin color the "generic"  
yellow. According to Femke Snelting the yellow color has the connotation of white skin color and not  
a respresentation of all skin tones. So because of this I should probably have had a slider to choose  
skin color as well. To work with different modifications like this really made me think, that when  
you modify the universal it is really not solving an issue, but rather opening up many other  
modifications to be done.  

In this exercise I have learned to use a couple of new codes, because I wanted to make my own  
shape and I wanted to make a slider, that could change the colors. These new learnings consist of:  

    colorMode()
    bezierVertex()
    beginContour()
    createSlider()
    slider.position()
    slider.style()

I learned how to change the color mode to HSB code, because the colors seem to be a little nicer  
than the RGB color codes. When making the hijab shape, I found out, how to make the exact shape that  
I wanted, and how to make a "hole" in a shape with beginContour().  
I learned how to use a slider, but I still find it hard to control what colors to be chosen within the slider.  

[Link to RUNME](https://freyjavm.gitlab.io/ap2020/MiniX2/)

[Link to index.html](https://gitlab.com/freyjavm/ap2020/-/blob/master/public/MiniX2/index.html)

[Link to sketch.js](https://gitlab.com/freyjavm/ap2020/-/blob/master/public/MiniX2/sketch.js)