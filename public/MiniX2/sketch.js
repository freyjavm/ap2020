let slider1;
let slider2;

function setup() {

  // Using HSB colors as they seem shaper
  colorMode(HSB);

  // Settings for slider for hijab emoji
  slider1 = createSlider(0, 360, 60, 40);
  slider1.position(455, 700);
  slider1.style('width', '80px');

  // Settings for slider for burka emoji
  slider2 = createSlider(0, 360, 60, 40);
  slider2.position(955, 700);
  slider2.style('width', '80px');

  createCanvas(windowWidth, windowHeight);
}

function draw() {

  background(255);

  // HERE THE HIJAB EMOJI BEGINS

  // Face (hijab emoji)
  fill(51, 100, 100)
  noStroke()
  ellipse(500, 350, 330, 400);

  // Mouth (hijab emoji)
  stroke(0);
  strokeWeight(3)
  noFill()
  curve(500, 400, 570, 461, 430, 461, 50, 50);

  // Nose (hijab emoji)
  stroke(0);
  noFill()
  curve(500, 300, 520, 400, 480, 400, 500, 300);

  // Right eye (hijab emoji)
  // vertex() is used to define direct lines.
  // bezierVertex() is a way to do a curved line
  // by having two control points, that determine
  // how the line is curved.
  fill(255)
  stroke(0)
  beginShape();
  vertex(610, 330);
  bezierVertex(600, 300, 550, 300, 535, 340);
  bezierVertex(550, 360, 600, 360, 610, 330);
  endShape();
  noStroke();
  fill(25, 86, 55);
  ellipse(573, 332, 36, 36);
  fill(0);
  ellipse(573, 332, 20, 20);

  // Left eye (hijab emoji)
  fill(255)
  stroke(0)
  beginShape();
  vertex(465, 340);
  bezierVertex(455, 300, 405, 300, 390, 330);
  bezierVertex(405, 360, 455, 360, 465, 340);
  endShape();
  noStroke();
  fill(25, 86, 55);
  ellipse(429, 331, 36, 36);
  fill(0);
  ellipse(429, 331, 20, 20);

  // Hijab (hijab emoji)
  let val1 = slider1.value();

  fill(val1, 100, 100, 1)
  stroke(0, 102, 0)
  beginShape();
  vertex(500, 145);
  bezierVertex(600, 150, 650, 210, 680, 340);
  bezierVertex(680, 390, 690, 420, 650, 580);
  bezierVertex(660, 600, 680, 610, 690, 700);
  vertex(300, 580)
  bezierVertex(300, 570, 310, 550, 330, 540);
  bezierVertex(310, 450, 310, 400, 320, 340);
  bezierVertex(330, 270, 390, 140, 500, 145);
  // The gap for the face (hijab emoji)
  beginContour();
  vertex(500, 230);
  vertex(340, 340);
  bezierVertex(340, 440, 380, 530, 500, 550);
  bezierVertex(620, 530, 660, 440, 660, 340);
  endContour();
  endShape(CLOSE);


  // HERE THE BURKA EMOJI BEGINS

  // Face (burka emoji)
  fill(51, 100, 100)
  noStroke()
  ellipse(1000, 350, 330, 400);

  // Right eye (burka emoji)
  fill(255)
  stroke(0)
  beginShape();
  vertex(1110, 330);
  bezierVertex(1100, 300, 1050, 300, 1035, 340);
  bezierVertex(1050, 360, 1100, 360, 1110, 330);
  endShape();
  noStroke();
  fill(25, 86, 55);
  ellipse(1073, 332, 36, 36);
  fill(0);
  ellipse(1073, 332, 20, 20);

  // Left eye (burka emoji)
  fill(255)
  stroke(0)
  beginShape();
  vertex(965, 340);
  bezierVertex(955, 300, 905, 300, 890, 330);
  bezierVertex(905, 360, 955, 360, 965, 340);
  endShape();
  noStroke();
  fill(25, 86, 55);
  ellipse(929, 331, 36, 36);
  fill(0);
  ellipse(929, 331, 20, 20);

  // Hijab (burka emoji)
  let val2 = slider2.value();

  fill(val2, 100, 100, 1)
  stroke(0, 102, 0)
  beginShape();
  vertex(1000, 145);
  bezierVertex(1100, 150, 1150, 210, 1180, 340);
  bezierVertex(1180, 390, 1190, 420, 1150, 580);
  bezierVertex(1160, 600, 1180, 610, 1190, 700);
  vertex(800, 580)
  bezierVertex(800, 570, 810, 550, 830, 540);
  bezierVertex(810, 450, 810, 400, 820, 340);
  bezierVertex(830, 270, 890, 140, 1000, 145);
  // The gap for the face (burka emoji)
  beginContour();
  vertex(1140, 280);
  bezierVertex(1040, 290, 960, 290, 860, 280);
  vertex(850, 380);
  bezierVertex(960, 395, 1040, 395, 1150, 380);
  vertex(1140, 280);
  endContour();
  endShape(CLOSE);


}
