function setup() {

  createCanvas(windowWidth, windowHeight, WEBGL);
  print("hello world");
  detailX = createSlider(3, 24, 3);
  detailX.position(10, height + 5);
  detailX.style('width', '80px');
}

function draw() {

  background(random(5));
  fill(255, 204, 0);

  // sun
  rotateY(millis() / 3000);
  sphere(100);

  // planets
  rotateY(millis() / 4000);
  fill(174, 176, 175);
  ellipse(150,10,20,20);

  rotateY(millis() / 2000);
  fill(179, 59, 0);
  ellipse(200,10,25,25);

  rotateY(millis() / 4000);
  fill(25, 102, 255);
  ellipse(250,10,30,30);

  rotateY(millis() / 4000);
  fill(255,195,77);
  ellipse(300,10,20,20);

  rotateY(millis() / 4000);
  fill(255,179,25);
  ellipse(360,10,70,70);

  rotateY(millis() / 4000);
  fill(212,194,106);
  ellipse(450,10,60,60);

  rotateY(millis() / 4000);
  fill(0,207,173);
  ellipse(500,10,30,30);

  rotateY(millis() / 4000);
  fill(0,170,255);
  ellipse(550,10,30,30);
}
