README on MiniX1

![ScreenShot](miniX1.png)

My very first try-out with programming illustrates a simple version of the solar system
with the sun and our 8 planets. I wanted to make all planets 3D, but I had to compromise
and make them flat ellipses.

My first independent coding experience was a little demanding.
As I am only learning the basics I have to accept that will need to compromise.
I found out that small things take a lot longer time than I expected.
It is easy to copy/paste a code, but what I found hard and time consuming was to understand
how I could control the code and how everything worked together.

Coding is similar to reading and writing to me, as you have different elements, that have different
functions and work in a specific way together. On the other hand it is different in the fact
that it is way more precise than the usual syntax. In usual syntax, you can have small mistakes,
but still be understandable. In coding you can't.

Programming has opened a world of creativity and new possibilities and I find that very exciting.

[Link to RUNME](http://freyjavm.gitlab.io/ap2020/MiniX1/)