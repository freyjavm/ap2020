let button;
let randomOutfit = 0;
let topColor;
let bottomColor;

function setup() {
  createCanvas(900, 520);
  topColor = color(random(255), random(255), random(255));
  bottomColor = color(random(255), random(255), random(255));
}

function draw() {
  background(181, 142, 168);

  // Title
  fill(0)
  textSize(52);
  textFont('Georgia');
  text('MY WARDROBE', 170, 90);

  // Heading
  fill(0)
  textSize(30);
  textFont('Georgia');
  text('OUTFIT OF THE DAY', 170, 140);

  // Styling the outfit button with CSS
  button = createButton('GET AN OUTFIT');
  button.style("font-size","1em");
  button.style("border-radius","12px");
  button.position(180, 280);
  button.style('background', 'pink')
  button.style('border', 'pink')
  button.style('padding', "10px 14px")
  button.mousePressed(pressed);

  // Conditional statement to draw either a dress, shirt/pants or shirt/skirt
  fill(topColor);
  if (randomOutfit == 1) {
    dress();
  } else if (randomOutfit == 2) {
    pants();
    fill(bottomColor)
    shirt();
  } else if (randomOutfit == 3) {
    skirt();
    fill(bottomColor)
    shirt();
  }

}

// Function that enables random color choosing of clothes
function pressed() {
  randomOutfit = floor(random(1, 4))
  topColor = color(random(255), random(255), random(255));
  bottomColor = color(random(255), random(255), random(255));
}

// Self-made shape with vertex() and bezierVertex()
function shirt() {
  stroke(1)
  beginShape();
  vertex(500, 200);
  vertex(540, 165);
  vertex(580, 160);
  bezierVertex(590, 170, 600, 170, 610, 160);
  vertex(650, 165);
  vertex(690, 200);
  vertex(665, 220);
  vertex(650, 205);
  vertex(640, 260);
  vertex(650, 300);
  vertex(540, 300);
  vertex(550, 260);
  vertex(540, 205);
  vertex(525, 220);
  vertex(500, 200);
  endShape();
}

// Self-made shape with vertex() and bezierVertex()
function dress() {
  stroke(1)
  beginShape();
  vertex(550, 164);
  vertex(580, 160);
  bezierVertex(590, 170, 600, 170, 610, 160);
  vertex(640, 164);
  bezierVertex(638, 175, 640, 190, 650, 205);
  vertex(640, 260);
  vertex(650, 400);
  vertex(540, 400);
  vertex(550, 260);
  vertex(540, 205);
  bezierVertex(550, 190, 552, 175, 550, 164);
  endShape();
}

// Self-made shape with vertex() and bezierVertex()
function skirt() {
  stroke(1)
  beginShape();
  vertex(635, 290);
  vertex(650, 400);
  vertex(540, 400);
  vertex(555, 290);
  vertex(635, 290);
  endShape();
}

// Self-made shape with vertex() and bezierVertex()
function pants() {
  stroke(1)
  beginShape();
  vertex(635, 290);
  vertex(645, 480);
  vertex(605, 480);
  vertex(595, 350);
  vertex(590, 480);
  vertex(545, 480);
  vertex(555, 290);
  vertex(635, 290);
  endShape();
}
