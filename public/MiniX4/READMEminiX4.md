<h2>MY WARDROBE - OUTFIT OF THE DAY</h2>

![Screenshot](MiniX4.png)

<h4>Description of program - conceptually and technically.</h4>

**Capture of data and datafication**  
The views on capturing data through the internet interactions are taking a higher focus on how our data  
is being captured and how people are constantly contributing to the economies that make the use of the  
internet into valuable data. Caroline Gerlitz and Anne Hermund talk about how a “like economy” is being  
formed when the user’s interactions on Facebook are being made into data that is being used to show to other  
users, which has the consequence that more traffic and engagement is being made. As a user you feel like you  
have no choice of saying no to your data being captured, if you want to be a part of (in this example)  
social media. The more you click the more you feel like you are being social online, but at the same time  
you contribute more and more to the industry of data capturing. Ironically, the datafication is growing and  
the general consumer wants to quantify more and more. There are apps for almost everything you can think of  
and I have made a program that is a product of the tendency of datafication of people’s daily struggles.

**Introduction to my program**  
The program is an illustration of a datafication of a personal wardrobe. The program chooses your daily  
outfit, so you are free of any worries and time used on a crisis of choosing what clothes to wear. The idea  
is to make a program where you can add in all your clothes items and an algorithm chooses a whole outfit for  
you. As I don’t have the skills of making that, I chose to make an illustration of the idea to emphasize the  
tendencies of people wanting to follow the stream of datafication. This is also why the colors of the outfit  
is being picked randomly in my program. 

**Description of syntax**  
The program consists of a title, a button, and four different clothes items made with beginShape()/endShape()  
and vertex()/bezierVertex(). I made every item a function, so you can hide it away and call it when you need  
it drawn. The button is styled with CSS and named “GET AN OUTFIT”. Every time you click this button the  
program makes a random colored outfit of either a shirt with a pair of pants or with a skirt, or a dress  
only. To make it choose between these combinations of items (so you don’t get i.e. a dress with a skirt) I  
had to make an if/else statement with a variable that I called randomOutfit. This variable takes a random  
number from 1 to 4 and by using the floor() function, the numbers will be round off to the lower number,  
so for example 4 is round off to 3. So, in the if/else statement it says if the random number is equal to 1  
it runs the function dress, if it chooses the number 2 it runs pants with a shirt, if it chooses 3 it takes  
shirt with a skirt. All this have to be called when you press the button, and therefore I used the function  
button.mousePressed(), where I added a function that I called pressed. And this function is then being called  
when you hit the button, which starts the function floor that creates a random number of 1, 2 or 3.  
The random color in the outfit is executed by two variables (topColor/bottomColor) which are choosing random  
values in the rgb color codes. This is done in function setup, so it only chooses a color once, while the  
program is running. 

To develop the program further it could be made as an app and made actually functionable. With this I mean  
that it should be possible adding your own items of clothes to the virtual wardrobe and then an algorithm  
would make suggestions for what to wear every day. The algorithm should be based on what colours fit together,  
the weather forecast and so on. Furthermore, the app should enable the user off seeing all the items and  
combinate in another way than the algorithm would.

This program is not only emphasizing how much is being quantified, but it also emphasizes all the things that  
are being lost when turning actions into data. When choosing clothes on the app (or if you buy clothes online)  
you lose the feel of the outfit and importance of it. 


<h4>Links: [RUNME](https://freyjavm.gitlab.io/ap2020/MiniX4/), [Sketch](https://gitlab.com/freyjavm/ap2020/-/blob/master/public/MiniX4/sketch.js), [Index](https://gitlab.com/freyjavm/ap2020/-/blob/master/public/MiniX4/index.html)</h4>
