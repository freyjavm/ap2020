let points = 150;
let level = 1;

function levelbar() {
  push();
  translate(-265, -70);
  fill("#77D0FF");
  rect(width/2-200, 185, 300, 25, 15);
  fill("#05004E");
  rect(width/2-200, 191.5, points, 12);
  fill("#77D0FF");
  ellipse(width/2-200, 198, 55, 55);

  //level
  textSize(30);
  textAlign(CENTER);
  fill("#05004E");
  text(level, width/2-202, 209);
  pop();
}

function nextLevel() {
  if (points == 380) {
   points = 60
   level++
  }
}

function gainPoints() {
  if (value == 100) {
    points + 10
  } else if (value == 200) {
    points + 20
  } else if (value == 300) {
    points + 30
  } else if (value == 400) {
    points + 40
  }
}
