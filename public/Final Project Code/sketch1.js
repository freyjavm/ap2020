let currentPoints = 40;
let level = 1;

let keyValue = 0;
let mouseValue = 0;
let clickValue = 0;

let randomDifficulty;
let value;
let randomQuestion = [];
let paragraphs;

let coins = []; // array for the number of coins visible on the screen

function preload() { //Preload to load the json-files before loading the page
  dataQuestions = loadJSON('questions.json');
}

function setup() {
  createCanvas(1350, 750);
  questions();
  // Create an Audio input
  mic = new p5.AudioIn();
  // start the Audio Input.
  // By default, it does not .connect() (to the computer speakers)
  mic.start();
}

function draw() {
  background(220);
  levelbar();
  businessSide();
  logoEyes();
  buttonNext();
  nextLevel();
  topUsers();
  micValue();

  for (let i = 0; i < coins.length; i++) { //objekter bliver ved med at tegnes
    coins[i].move();
    coins[i].show();
    let overlapping = false;
    for (var j = 0; j < coins.length; j++) {
      if (coins[i] !== coins[j] && coins[i].coinsTouching(coins[j]) && coins[j].speed == 0) {
        overlapping = true;

      }
      if (coins[i].pos.y >= height - coins[i].size/2) {
        coins[i].speed = 0;
      } else {
        coins[i].speed = random(1,2);
      }
      if (overlapping) {
        coins[i].speed = 0;
      }

    }

  }
}

class Coin {
  constructor() { //svarer til objektets setup
    this.speed = random(1,2);
    this.size = (25,25);
    this.pos = new createVector(random(1015,1395),105);
  }
  move() {
    this.pos.y = this.pos.y + this.speed;
  }
  show() {
    push();
    stroke('#FFDB33');
    strokeWeight(3);
    fill('#FFF333');
    ellipse(this.pos.x, this.pos.y, this.size, this.size);
    textSize(14);
    textAlign(CENTER);
    fill('#FFDB33');
    stroke(0); //evt orange
    strokeWeight(1);
    text("$", this.pos.x, this.pos.y+4.5);
    pop();
  }

  coinsTouching(other){
    //if the distance is less than the radius of this bubble and another bubble, they are overlapping
    let d = dist(this.pos.x,this.pos.y,other.pos.x,other.pos.y);
    if (d < this.size/3) {
      return true;
    } else {
      return false;
    }
  }

}

function logoEyes() {
  noStroke();
  fill(60, 90, 255);
  rect(0,0,1400,100);
  stroke(0);
  fill(0);
  textAlign(CENTER);
  text("DataBook", width/2, 90);
  textSize(100);

  movingEyeX1 = map(mouseX, 0, width, 750, 770);
  movingEyeX2 = map(mouseX, 0, width, 805, 825);
  movingEyeY = map(mouseY, 0, height, 67, 80);

  fill(0);
  ellipse(movingEyeX1,movingEyeY,17,17);
  ellipse(movingEyeX2,movingEyeY,17,17);
}

function questions() {
  let yPos=100;
  let paragraphs = [];
  let oldParagraphs = [];
  let inputFields = [];
  let points = [];
  let buttons = [];

  //jeg har slettet let under for-loopet, fordi det ikke var nødvendingt, da det bare er definitioner, tror jeg. ændrer sig måske med next-button
  for (let i =0; i < 4; i++) {
    randomDifficulty = random([0, 1, 2, 3, 4]);
    questions = dataQuestions.allQuestions[randomDifficulty].questions;
    value = dataQuestions.allQuestions[randomDifficulty].value;
    randomQuestion = random(questions);
    paragraphs[i] = randomQuestion;
    text1 = createP(randomQuestion);
    text1.position(100,yPos*i+200);
    print(paragraphs[i]);
    //print(paragraphs);

    //inputfelter
    inputFields[i] = createInput("")
    .position(100,50+yPos*i+200)
    .size(400,15)
    .value("")
    .attribute('placeholder', 'Earn x points here'.replace("x",value));

    //antal point spørgsmålet giver. Er placeret bag contribute-button
    points[i] = createP("+"+value)
    .position(540,235+i*100);

    //contribute buttons
    buttons[i] = createButton("Contribute")
    .position(530,250+i*100)
    .size(75,20)
    .style('background-color', "#05004E")
    .value(value)
    // Gemmer knappen i sin egen variabel, som ikke bliver overskrevet. Når knappen bliver trykket på, så kører den
    // Funktionen herunder. (Det eneste funktion gør, er at sende knappen videre til din rigtige funktion.
    .mousePressed(function(){ buttonPressed(buttons[i], inputFields[i], paragraphs[i], i)});

    nextButton = createButton('Next')
    .style('background-color', 'blue')
    .style("border-radius","12px")
    .style('border', 'blue')
    .style('padding', "10px 14px")
    .style('font-size', '25px')
    .position(320,600)
    .mousePressed(function(){ newQuestions(buttons[i], inputFields[i], paragraphs[i], points[i], i, oldParagraphs)});
    // nextButton.mousePressed(function(){ questions(buttons[i], inputFields[i], paragraphs[i], points[i], i)});
  }
}

function buttonPressed(contributeButton, inputFields, i) {
  let input = inputFields.value();
  print(inputFields.value())
  if (input == "") {
    alert("Please type an answer");
    return false;
  }
  else {
    contributeButton.hide();
    currentPoints = (contributeButton.value())/10 + currentPoints;
    // EVT LÅS INPUT
    return true;
  }
}
function newQuestions(buttons, inputFields, paragraphs, points, i, oldParagraphs) {
for (var i = 0; i < 4; i++) {
text1.hide();
  paragraphs[i].splice(i);
}
//print(paragraphs);

  // paragraphs0.remove();
  // paragraphs1.remove();
  // paragraphs2.remove();
  // paragraphs3.remove();
print(oldParagraphs);
}

function topUsers() {
  let userX = 670
  let userY = 310

  // EVT GØR NOGET VED, AT DET BLIVER LOOPET OG SKREVET I FED
  push();
  textSize(22);
  fill("#05004E");
  textAlign(CENTER);
  text("TOP 5 USERS", 820, 270);
  strokeWeight(2)
  line(720, 280, 919, 280);
  pop();

  // Top1
  // Username
  push();
  textSize(15);
  fill("#05004E");
  textAlign(LEFT);
  text("Linda Johnson", userX, userY);

  // Level bar
  fill("#77D0FF");
  rect(userX+170, userY-12, 125, 13, 10);
  fill("#05004E");
  rect(userX+170, userY-8, 105, 5, 10);
  fill("#77D0FF");
  ellipse(userX + 170, userY-6, 38, 28);

  // Level number
  textSize(17);
  textAlign(CENTER);
  fill("#05004E");
  text("665", userX+170, userY);
  pop();

  // Top2
  push();
  translate(0, 50);
  // Username
  textSize(15);
  fill("#05004E");
  textAlign(LEFT);
  text("Robert Smith", userX, userY);

  // Level bar
  fill("#77D0FF");
  rect(userX+170, userY-12, 125, 13, 10);
  fill("#05004E");
  rect(userX+170, userY-8, 30, 5, 10);
  fill("#77D0FF");
  ellipse(userX + 170, userY-6, 38, 28);

  // Level number
  textSize(17);
  textAlign(CENTER);
  fill("#05004E");
  text("597", userX+170, userY);
  pop();

  // Top3
  push();
  translate(0, 100);
  // Username
  textSize(15);
  fill("#05004E");
  textAlign(LEFT);
  text("Maria Rogers", userX, userY);

  // Level bar
  fill("#77D0FF");
  rect(userX+170, userY-12, 125, 13, 10);
  fill("#05004E");
  rect(userX+170, userY-8, 75, 5, 10);
  fill("#77D0FF");
  ellipse(userX + 170, userY-6, 38, 28);

  // Level number
  textSize(17);
  textAlign(CENTER);
  fill("#05004E");
  text("572", userX+170, userY);
  pop();

  // Top4
  push();
  translate(0, 150);
  // Username
  textSize(15);
  fill("#05004E");
  textAlign(LEFT);
  text("Jane Coleman Wright", userX, userY);

  // Level bar
  fill("#77D0FF");
  rect(userX+170, userY-12, 125, 13, 10);
  fill("#05004E");
  rect(userX+170, userY-8, 90, 5, 10);
  fill("#77D0FF");
  ellipse(userX+170, userY-6, 38, 28);

  // Level number
  textSize(17);
  textAlign(CENTER);
  fill("#05004E");
  text("565", userX+170, userY);
  pop();

  // Top5
  push();
  translate(0, 200);
  // Username
  textSize(15);
  fill("#05004E");
  textAlign(LEFT);
  text("Martin Cook Morris", userX, userY);

  // Level bar
  fill("#77D0FF");
  rect(userX+170, userY-12, 125, 13, 10);
  fill("#05004E");
  rect(userX+170, userY-8, 40, 5, 10);
  fill("#77D0FF");
  ellipse(userX+170, userY-6, 38, 28);

  // Level number
  textSize(17);
  textAlign(CENTER);
  fill("#05004E");
  text("501", userX+170, userY);
  pop();
}

function levelbar() {
  push();
  translate(-265, -70);
  fill("#77D0FF");
  rect(width/2-200, 185, 300, 25, 15);
  fill("#05004E");
  rect(width/2-200, 191.5, currentPoints, 12, 15);
  fill("#77D0FF");
  ellipse(width/2-200, 198, 70, 50);

  //level
  textSize(30);
  textAlign(CENTER);
  fill("#05004E");
  text(level, width/2-202, 209);
  pop();
}

function nextLevel() {
  if (currentPoints >= 290) {
    currentPoints = 40;
    level++;
    print(currentPoints);
  }
}

function micValue() {
  let volValue = mic.getLevel();
  let mappedVol = floor(map(volValue,0,1,0,15));
  if (mappedVol > 2.5){
    coins.push(new Coin);
  }

}

function keyPressed() {
  keyValue++;
  if(keyValue > 1) {
    coins.push(new Coin);
    keyValue = 0;
  }
}

function mouseMoved() {
  mouseValue++;
  if(mouseValue > 20) {
    coins.push(new Coin);
    mouseValue = 0;
  }
  return false;
}

function mouseClicked(){
  coins.push(new Coin);
  return false;
}

function businessSide() {
  rect(1000, 0, 350, 750);
  if (mouseX > 1000) {
    noCursor();
  } else {
    cursor('default');
  }
}

function buttonNext (){
  // nextButton = createButton('Next');
  // nextButton.style('background-color', 'blue');
  // nextButton.style("border-radius","12px");
  // nextButton.style('border', 'blue')
  // nextButton.style('padding', "10px 14px")
  // nextButton.style('font-size', '25px');
  // nextButton.position(320,600);
  // nextButton.mousePressed(newQuestions);
}


// function ButtonNextPushed() {
//  Stine -  questions.splice(variabel for det spørsmål der bliver valgt,1);
//  Olivia
// array[i].splice(value);
//
// for(i=0;i<array.lenght; i++);
// array[].push(value / i);
// array[].push(random())

// }
