let currentPoints = 40;
let level = 1;

let keyValue = 0;
let mouseValue = 0;
let clickValue = 0;

let randomDifficulty;
let value;
let randomQuestion = [];
let paragraphs;

let coins = []; // array for the number of coins visible on the screen

function preload() { //Preload to load the json-files before loading the page
  dataQuestions = loadJSON('questions.json');
}

function setup() {
  createCanvas(1350, 750);
  questions();
  // Create an Audio input
  mic = new p5.AudioIn();
  // start the Audio Input.
  // By default, it does not .connect() (to the computer speakers)
  mic.start();
}

function draw() {
  background("#3d6098");
  levelbar();
  businessSide();
  logoEyes();
  buttonNext();
  nextLevel();
  topUsers();
  micValue();
  wall();

  for (let i = 0; i < coins.length; i++) { //objekter bliver ved med at tegnes
    coins[i].move();
    coins[i].show();
    let overlapping = false;
    for (var j = 0; j < coins.length; j++) {
      if (coins[i] !== coins[j] && coins[i].coinsTouching(coins[j]) && coins[j].speed == 0) {
        overlapping = true;

      }
      if (coins[i].pos.y >= height - coins[i].size/2) {
        coins[i].speed = 0;
      } else {
        coins[i].speed = random(1,2);
      }
      if (overlapping) {
        coins[i].speed = 0;
      }

    }

  }
}

class Coin {
  constructor() { //svarer til objektets setup
    this.speed = random(1,2);
    this.size = (25,25);
    this.pos = new createVector(random(1015,1395),105);
  }
  move() {
    this.pos.y = this.pos.y + this.speed;
  }
  show() {
    push();
    stroke('#FFDB33');
    strokeWeight(3);
    fill('#FFF333');
    ellipse(this.pos.x, this.pos.y, this.size, this.size);
    textSize(14);
    textAlign(CENTER);
    fill('#FFDB33');
    stroke(0); //evt orange
    strokeWeight(1);
    text("$", this.pos.x, this.pos.y+4.5);
    pop();
  }

  coinsTouching(other){
    //if the distance is less than the radius of this bubble and another bubble, they are overlapping
    let d = dist(this.pos.x,this.pos.y,other.pos.x,other.pos.y);
    if (d < this.size/3) {
      return true;
    } else {
      return false;
    }
  }

}

function logoEyes() {
  noStroke();
  fill("#FFFFFF");
  rect(0,0,1400,80);
  stroke(0);
  fill("#213159");
  textFont('Verdana');
  textAlign(CENTER);
  text("QUESTION ROOM", width/2, 62);
  textSize(50);

  movingEyeX1 = map(mouseX, 0, width, 791, 800);
  movingEyeX2 = map(mouseX, 0, width, 831, 840);
  movingEyeY = map(mouseY, 0, height, 45, 52);

push();
  noStroke();
  fill("#213159");
  ellipse(movingEyeX1,movingEyeY,15,15);
  ellipse(movingEyeX2,movingEyeY,15,15);
pop();

}

function questions() {
  let yPos=100;
  let paragraphs = [];
  let inputFields = [];
  let points = [];
  let buttons = [];

  //jeg har slettet let under for-loopet, fordi det ikke var nødvendingt, da det bare er definitioner, tror jeg. ændrer sig måske med next-button
  for (let i =0; i < 4; i++) {
    randomDifficulty = random([0, 1, 2, 3, 4]);
    questions = dataQuestions.allQuestions[randomDifficulty].questions;
    value = dataQuestions.allQuestions[randomDifficulty].value;
    randomQuestion = random(questions);
    paragraphs[i] = randomQuestion;
    text1 = createP(randomQuestion);
    text1.position(60,yPos*i+200)
    text1.style ('color',"#FFFFFF")
    text1.style('font-family', 'Verdana');
    print(paragraphs[i]);

    //inputfelter
    inputFields[i] = createInput("")
    .position(60,50+yPos*i+200)
    .size(440,15)
    .value("")
    .attribute('placeholder', 'Earn x points here'.replace("x",value));

    //antal point spørgsmålet giver. Er placeret bag contribute-button
    points[i] = createP("+"+value)
    .position(540,235+i*100);

    //contribute buttons
    buttons[i] = createButton("CONTRIBUTE")
    .position(530,250+i*100)
    .size(85,24)
    .style('background-color', "#05004E")
    .value(value)
    .style('color', "#FFFFFF")
    .style('border', 'none')
    .style('text-align', 'center')
    .style('padding', "5px 5px")
    .style('border-radius', "8px")
    // Gemmer knappen i sin egen variabel, som ikke bliver overskrevet. Når knappen bliver trykket på, så kører den
    // Funktionen herunder. (Det eneste funktion gør, er at sende knappen videre til din rigtige funktion.
    .mousePressed(function(){ buttonPressed(buttons[i], inputFields[i], i)});

    nextButton = createButton('NEXT')
    .size(85,24)
    .style('background-color', "#05004E")
    .style('color', "#FFFFFF")
    .style("border-radius","8px")
    .style('border', 'none')
    .style('padding', "2px 2px")
    .style('font-size', '15px')
    .style('text-align', 'center')
    .position(60,630) //530
    .mousePressed(function(){ newQuestions(buttons[i], inputFields[i], paragraphs[i], points[i], i)});
    // .mousePressed(function(){ questions(buttons[i], inputFields[i], paragraphs[i], points[i], i)});
  }
}

function buttonPressed(contributeButton, inputFields, i) {
  let input = inputFields.value();
  print(inputFields.value());
  if (input == "") {
    alert("Please type an answer");
    return false;
  }
  else {
    contributeButton.hide();
    currentPoints = (contributeButton.value())/10 + currentPoints;'opacity', '0.6', 'cursor', 'not-allowed'
    // EVT LÅS INPUT
    return true;
  }
}
function newQuestions(buttons, inputFields, paragraphs, points, i) {
  let oldParagraphs = paragraphs.splice(0);

  //print(paragraphs);

  // paragraphs0.remove();
  // paragraphs1.remove();
  // paragraphs2.remove();
  // paragraphs3.remove();
  print(oldParagraphs);
}

function topUsers() {
  let userX = 710
  let userY = 285

  // EVT GØR NOGET VED, AT DET BLIVER LOOPET OG SKREVET I FED
  push();
  textSize(22);
  fill("#05004E");
  textAlign(CENTER);
  text("TOP USERS", 820, 235);
  strokeWeight(2)
  line(720, 248, 919, 248);
  pop();

  // Top1
  // Username
  push();
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Linda Johnson", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 80, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("665", userX+130, userY);
  pop();

  // Top2
  push();
  translate(0, 40);
  // Username
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Robert Smith", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 20, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("597", userX+130, userY);
  pop();

  // Top3
  push();
  translate(0, 80);
  // Username
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Maria Rogers", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 60, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("572", userX+130, userY);
  pop();

  // Top4
  push();
  translate(0, 120);
  // Username
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Jane Coleman Wright", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 50, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("565", userX+130, userY);
  pop();

  // Top5
  push();
  translate(0, 160);
  // Username
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Martin Cook Morris", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 85, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("501", userX+130, userY);
  pop();

  // Top6
  push();
  translate(0, 200);
  // Username
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Sally Reed Gray", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 65, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("484", userX+130, userY);
  pop();

  // Top7
  push();
  translate(0, 240);
  // Username
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Lisa Nelson", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 30, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("483", userX+130, userY);
  pop();

  // Top8
  push();
  translate(0, 280);
  // Username
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Jack James Collins", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 40, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("439", userX+130, userY);
  pop();
}

function levelbar() {
  push();
  translate(-265, -70);
  fill("#B2DAFF");
  rect(width/2-200, 185, 300, 25, 15);
  fill("#05004E");
  rect(width/2-200, 191.5, currentPoints, 12, 15);
  fill("#B2DAFF");
  ellipse(width/2-200, 198, 70, 50);

  //level
  textSize(30);
  textAlign(CENTER);
  fill("#05004E");
  text(level, width/2-202, 209);
  pop();
}

function nextLevel() {
  if (currentPoints >= 290) {
    currentPoints = 40;
    level++;
    print(currentPoints);
  }
}

function micValue() {
  let volValue = mic.getLevel();
  let mappedVol = floor(map(volValue,0,1,0,15));
  if (mappedVol > 2.5){
    coins.push(new Coin);
  }

}

function keyPressed() {
  keyValue++;
  if(keyValue > 1) {
    coins.push(new Coin);
    keyValue = 0;
  }
}

function mouseMoved() {
  mouseValue++;
  if(mouseValue > 20) {
    coins.push(new Coin);
    mouseValue = 0;
  }
  return false;
}

function mouseClicked(){
  coins.push(new Coin);
  return false;
}

function wall() {
  push();
  fill(220);
  rect(990, 80, 10, 680);
  pop();
}

function businessSide() {
  rect(1000, 0, 350, 750);
  if (mouseX > 1000) {
    noCursor();
  } else {
    cursor('default');
  }
}

function buttonNext (){
  // nextButton = createButton('Next');
  // nextButton.style('background-color', 'blue');
  // nextButton.style("border-radius","12px");
  // nextButton.style('border', 'blue')
  // nextButton.style('padding', "10px 14px")
  // nextButton.style('font-size', '25px');
  // nextButton.position(320,600);
  // nextButton.mousePressed(newQuestions);
}


// function ButtonNextPushed() {
//  Stine -  questions.splice(variabel for det spørsmål der bliver valgt,1);
//  Olivia
// array[i].splice(value);
//
// for(i=0;i<array.lenght; i++);
// array[].push(value / i);
// array[].push(random())

// }
