function setup() {
  createCanvas(1350, 750);
  background(255);
  topUsers();
  inputFields();
  buttons();
}


function inputFields() {
let inputX=100
let inputY=250
}

function buttons() {
let buttonX=530
let buttonY=250
}

function topUsers() {
  let userX = 670
  let userY = 310

  push();
    textSize(22);
    fill("#05004E");
    textAlign(CENTER);
    text("TOP 5 USERS", 820, 270);
    strokeWeight(2)
    line(720, 280, 919, 280);
  pop();

  // Top1
    // Username
    textSize(15);
    fill("#05004E");
    textAlign(LEFT);
    text("Linda Johnson", userX, userY);

    // Level bar
    fill("#77D0FF");
    rect(userX+170, userY-12, 125, 13, 10);
    fill("#05004E");
    rect(userX+170, userY-8, 105, 5);
    fill("#77D0FF");
    ellipse(userX + 170, userY-6, 38, 28);

    // Level number
    textSize(17);
    textAlign(CENTER);
    fill("#05004E");
    text("665", userX+170, userY);

  // Top2
  push();
    translate(0, 50);
      // Username
      textSize(15);
      fill("#05004E");
      textAlign(LEFT);
      text("Robert Smith", userX, userY);

      // Level bar
      fill("#77D0FF");
      rect(userX+170, userY-12, 125, 13, 10);
      fill("#05004E");
      rect(userX+170, userY-8, 30, 5);
      fill("#77D0FF");
      ellipse(userX + 170, userY-6, 38, 28);

      // Level number
      textSize(17);
      textAlign(CENTER);
      fill("#05004E");
      text("597", userX+170, userY);
  pop();

  // Top3
  push();
    translate(0, 100);
      // Username
      textSize(15);
      fill("#05004E");
      textAlign(LEFT);
      text("Maria Rogers", userX, userY);

      // Level bar
      fill("#77D0FF");
      rect(userX+170, userY-12, 125, 13, 10);
      fill("#05004E");
      rect(userX+170, userY-8, 75, 5);
      fill("#77D0FF");
      ellipse(userX + 170, userY-6, 38, 28);

      // Level number
      textSize(17);
      textAlign(CENTER);
      fill("#05004E");
      text("572", userX+170, userY);
  pop();

  // Top4
  push();
    translate(0, 150);
      // Username
      textSize(15);
      fill("#05004E");
      textAlign(LEFT);
      text("Jane Coleman Wright", userX, userY);

      // Level bar
      fill("#77D0FF");
      rect(userX+170, userY-12, 125, 13, 10);
      fill("#05004E");
      rect(userX+170, userY-8, 90, 5);
      fill("#77D0FF");
      ellipse(userX+170, userY-6, 38, 28);

      // Level number
      textSize(17);
      textAlign(CENTER);
      fill("#05004E");
      text("565", userX+170, userY);
  pop();

  // Top5
  push();
    translate(0, 200);
      // Username
      textSize(15);
      fill("#05004E");
      textAlign(LEFT);
      text("Martin Cook Morris", userX, userY);

      // Level bar
      fill("#77D0FF");
      rect(userX+170, userY-12, 125, 13, 10);
      fill("#05004E");
      rect(userX+170, userY-8, 40, 5);
      fill("#77D0FF");
      ellipse(userX+170, userY-6, 38, 28);

      // Level number
      textSize(17);
      textAlign(CENTER);
      fill("#05004E");
      text("501", userX+170, userY);
  pop();
}
